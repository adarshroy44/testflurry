//
//  main.m
//  FlurryProj
//
//  Created by Adarsh on 28/06/16.
//  Copyright © 2016 Indusnet Technology. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
