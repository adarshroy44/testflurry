//
//  ViewController.m
//  FlurryProj
//
//  Created by Adarsh on 28/06/16.
//  Copyright © 2016 Indusnet Technology. All rights reserved.
//

#import "ViewController.h"
#import "Flurry.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)firstBtnAction:(UIButton*)btn
{
    [Flurry logEvent:@"ButtonTap FirstButton"];
}

@end
